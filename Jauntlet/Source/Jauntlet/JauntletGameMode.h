// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "JauntletGameMode.generated.h"

UCLASS(minimalapi)
class AJauntletGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AJauntletGameMode();

	class UUMG_HUD* UMG_HUD; //forward decl

	UPROPERTY( BlueprintReadOnly, EditDefaultsOnly, Category = HUD )
	TSubclassOf<class UUMG_HUD> HUDToSpawn;


	//APlayerController* p1, p2;
	virtual void BeginPlay() override;
	virtual void EndPlay ( const EEndPlayReason::Type EndPlayReason ) override;	    
	
	void GM_IncrementRound ();
	void GM_IncrementP1Wins ();
	void GM_IncrementP2Wins ();

	void GM_FinishRound () { roundFinished = true; }
	bool GM_GetRoundFinished () { return roundFinished;  }

	int32 GM_GetRoundNum () { return roundNum; }
	int32 GM_GetP1Wins () { return p1Wins; }
	int32 GM_GetP2Wins () { return p2Wins; }

private:
	const int MAX_ROUND = 7;
	const int MAX_WINS = 4;
	int32 roundNum = 1; // Round # on the HUD will always update to this variable
	int32 p1Wins = 0;
	int32 p2Wins = 0;
	bool roundFinished = false;
};
