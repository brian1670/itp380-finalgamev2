// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Jauntlet.h"
#include "JauntletCharacter.h"
#include "JauntletProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "EngineUtils.h"
#include "Lance.h"
#include "JauntletGameMode.h"


DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AJauntletCharacter

AJauntletCharacter::AJauntletCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
    
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = GetCapsuleComponent();
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->AttachParent = FirstPersonCameraComponent;
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);                    // only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->AttachTo(Mesh1P, TEXT("GripPoint"), EAttachLocation::SnapToTargetIncludingScale, true);

	MyLance = nullptr;
    
	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 30.0f, 10.0f);
	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P are set in the
	// derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	movementMode = Joust;    
}

void AJauntletCharacter::BeginPlay ()
{
	Super::BeginPlay();
	// Spawn the lance, if one was specified
	if ( LanceClass )
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Set rotation if not properly aligned
			FRotator Rotation(0.0f, 90.0f, 0.0f);
			// Spawn the Lance
			//for (TActorIterator<ALance> ActorItr(GetWorld()); ActorItr; ++ActorItr )
			//{
			//	if ( ActorItr->ActorHasTag( "lance1" ) )
			//	{
			//		MyLance = *ActorItr;
			//	}
			//}
			GetCharacterMovement ()->MaxWalkSpeed = 0;
			MyLance = World->SpawnActor<ALance>(LanceClass, FVector::ZeroVector, Rotation, SpawnParams);
			if ( MyLance )
			{
				MyLance->GetLanceMesh ()->AttachTo ( GetMesh (), TEXT ( "LanceSocket" ) );
				MyLance->SetActorRelativeLocation(FVector(-25.0f, 0.0f, 50.0f));
                MyLance->SetOwner(this);
				FName lanceTag = ActorHasTag ( "p1" ) ? "l1" : "l2";	// tag the lance so collision can detect
				MyLance->Tags.Add ( lanceTag );
				UE_LOG(LogTemp, Log, TEXT("Success attaching lance"));
			}
			else
			{
				UE_LOG(LogTemp, Log, TEXT("Failed to attach lance"));
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AJauntletCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Fire", IE_Pressed, this, &AJauntletCharacter::OnFire);

	/*
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AJauntletCharacter::TouchStarted);
	if( EnableTouchscreenMovement(InputComponent) == false )
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AJauntletCharacter::OnFire);
	}
	
	InputComponent->BindAxis("MoveForward", this, &AJauntletCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AJauntletCharacter::MoveRight);
	
	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &AJauntletCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &AJauntletCharacter::LookUpAtRate);
	*/
}
 

void AJauntletCharacter::Tick(float deltaTime)
{
    Super::Tick(deltaTime);

	if(GetCharacterMovement()->MaxWalkSpeed >= 0) 
	{
		GetCharacterMovement()->MaxWalkSpeed -= 200*deltaTime;
		GetCharacterMovement()->MaxWalkSpeed = FMath::Clamp(GetCharacterMovement()->MaxWalkSpeed, 0.0f, 700.0f);
	}
    
    AGameMode* GameMode = GetWorld()->GetAuthGameMode(); // Get the GameMode
    AJauntletGameMode* JGameMode = Cast<AJauntletGameMode>( GameMode ); //downcast
    
    if(JGameMode->GM_GetP1Wins() >= 4  || JGameMode->GM_GetP2Wins() >= 4)
    {
        mGameEnded = true;
    }
    
	if ( movementMode == Joust && !mGameEnded ) // If jousting, put the players on rails
	{
		AddMovementInput( GetActorForwardVector(), 1.0f ); // constantly move forward

		if ( MyLance )
		{
			if ( xRate != 0.0f || yRate != 0.0f )
			{
				FVector lancePos = MyLance->GetMovementComponent ()->GetActorLocation ();
				//lancePos.X = xRate * deltaTime;
				lancePos.X += yRate * deltaTime;
				//MyLance->SetActorRelativeLocation ( lancePos );
			}
		}
	}    
}

void AJauntletCharacter::OnFire()
{ 
	float addOnSpeed = 100;
	GetCharacterMovement ()->MaxWalkSpeed += addOnSpeed;
	/*
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		const FRotator SpawnRotation = GetControlRotation();
		// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
		const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);

		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			// spawn the projectile at the muzzle
			World->SpawnActor<AJauntletProjectile> ( ProjectileClass, SpawnLocation, SpawnRotation );
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if(FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if(AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
	*/
}

// Y Axis on Left Joystick
void AJauntletCharacter::MoveForward(float Value)
{
    //if ( movementMode == Roam )
    //{
    //    if ( Value != 0.0f )
    //    {
    //        // add movement in that direction
    //        AddMovementInput( GetActorForwardVector(), Value );
    //    }
    //}
    //else if ( movementMode == Joust )
    //{
    //    // Move shield up/down
	//	if ( MyLance )
	//	{
	//		if ( Value != 0.0f )
	//		{
	//			//MyLance->AddMovementInput ( FVector ( 0.0f, 1.0f, 0.0f ), Value );
	//		}
	//	}
    //}
}

// X Axis on Left Joystick
void AJauntletCharacter::MoveRight(float Value)
{
    //if ( movementMode == Roam )
    //{
    //    if ( Value != 0.0f )
    //    {
    //        // add movement in that direction
    //        AddMovementInput( GetActorRightVector(), Value );
    //    }
    //}
    //else if ( movementMode == Joust )
    //{
    //    // Move shield left/right
	//	if ( MyLance )
	//	{
	//		if ( Value != 0.0f )
	//		{
	//			//AddMovementInput( GetActorRightVector(), Value );
	//			//MyLance->AddMovementInput ( FVector ( 0.0f, 0.0f, 1.0f ), Value );
	//		}
	//	}
    //}
}

// X Axis on Right Joystick
void AJauntletCharacter::TurnAtRate(float Rate)
{
    if ( movementMode == Roam )
    {
        // calculate delta for this frame from the rate information
        AddControllerYawInput( Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds() );
    }
    else if ( movementMode == Joust )
    {
        // Move lance left/right
		//MyLance->MoveHorizontal ( Rate );

		if ( Rate != 0.0f )
		{
			if ( MyLance )
			{
				//UE_LOG(LogTemp, Log, TEXT( "Moving Horizontal: %f" ), Rate );
				//MyLance->MoveHorizontal ( Rate );
			}
		}
		xRate = Rate;
    }
}

// Y Axis on Right Joystick
void AJauntletCharacter::LookUpAtRate(float Rate)
{
	UE_LOG(LogTemp, Log, TEXT( "Y AXIS" ));
    if ( movementMode == Roam )
    {
	    // calculate delta for this frame from the rate information
		AddControllerPitchInput ( Rate * BaseLookUpRate * GetWorld ()->GetDeltaSeconds () );
    }
    else if ( movementMode == Joust )
    {
        // Move lance up/down
		if ( Rate != 0.0f )
		{
			if ( MyLance )
			{
				//MyLance->SetActorRelativeLocation( FVector( 0.0f, Rate * 1.0f, 0.0f));
				//MyLance->MoveVertical ( Rate );
			}
		}
		yRate = Rate;
    }
}