// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Lance.generated.h"

UCLASS()
class JAUNTLET_API ALance : public APawn
{
	GENERATED_BODY()
protected:
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    class USoundCue* DeathSound;
private:
    class AJauntletCharacter* mOwner;
public:
	// Sets default values for this pawn's properties
	ALance();
    
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void MoveHorizontal ( float AxisValue );
	void MoveVertical ( float AxisValue );
	void ResetCollision () { hasCollided = false; }
    void SetOwner(AJauntletCharacter* owner) { mOwner = owner; }
    
	virtual UPawnMovementComponent* GetMovementComponent() const override;

	USkeletalMeshComponent* GetLanceMesh(){ return LanceMesh; }
	
	UFUNCTION()
	void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

    UAudioComponent* PlayDeathSound(USoundCue* Sound);
    
    UPROPERTY(Transient)
    class UAudioComponent* DeathAC;
    
protected:
	UPROPERTY( VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon )
	USkeletalMeshComponent* LanceMesh;
	
private:
	float moveSpeed = 200.0f;
	class ULanceMovementComponent* MyMovementComponent;

	bool hasCollided = false;
};