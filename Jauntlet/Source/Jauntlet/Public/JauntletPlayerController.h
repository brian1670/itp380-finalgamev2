// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "JauntletPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class JAUNTLET_API AJauntletPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
    AJauntletPlayerController();
    
protected:
    // Begin PlayerController interface
    virtual void PlayerTick(float DeltaTime) override;
    virtual void SetupInputComponent() override;
    // End PlayerController interface
	
public:
    void Jump();
    void StopJumping();
    void MoveForward(float Val);
    void MoveRight(float Val);
    void AddControllerYawInput(float Val);
    void TurnAtRate(float Rate);
    void AddControllerPitchInput(float Val);
    void LookUpAtRate(float Rate);
    virtual void BeginPlay() override;
    
};
