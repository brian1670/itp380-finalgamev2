// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Jauntlet.h"
#include "JauntletGameMode.h"
#include "JauntletHUD.h"
#include "JauntletCharacter.h"
#include "UMG_HUD.h"

AJauntletGameMode::AJauntletGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
    static ConstructorHelpers::FClassFinder<AJauntletCharacter> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_JauntletCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
    
	// use our custom HUD class
	HUDClass = AJauntletHUD::StaticClass();
}

void AJauntletGameMode::GM_IncrementRound () 
{ 	
	if ( p1Wins >= MAX_WINS || p2Wins >= MAX_WINS ) // Game is over
	{
		int32 winner = ( p1Wins > p2Wins ) ? 1 : 2; // 1 is p1 won and 2 if p2 won
		UMG_HUD->ShowWinner ( winner );
	}
	else
	{
		roundNum++; 
	}
	roundFinished = false;
}

void AJauntletGameMode::GM_IncrementP1Wins () 
{ 
	p1Wins++; 
	GM_IncrementRound ();
}

void AJauntletGameMode::GM_IncrementP2Wins () 
{ 
	p2Wins++;
	GM_IncrementRound ();
}

void AJauntletGameMode::BeginPlay()
{
    Super::BeginPlay();
    for(int i = 0; i < 2; ++i)
    {
        UGameplayStatics::CreatePlayer(GetWorld(), i);
    }
	
	// Create the HUD and add it to the viewport
	UMG_HUD = CreateWidget<UUMG_HUD> ( GetWorld (), HUDToSpawn );
	UMG_HUD->AddToViewport ( 0 );
}

void AJauntletGameMode::EndPlay ( const EEndPlayReason::Type EndPlayReason )
{
	// Destroy the HUD
	UMG_HUD->RemoveFromParent ();
}